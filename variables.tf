variable "project_name" {
  description = "Project name. Can not be empty"
  type        = string
}

variable "project_id" {
  description = "Project id. Choose a short and easy one."
  type        = string
}

variable "environment" {
  description = "Environment for the projec: development, qa, production"
  default     = ""
  type        = string
}

variable "region" {
  description = "Region for the project. Defaults to 'europe-west1'. Full list at https://cloud.google.com/compute/docs/regions-zones"
  default     = "europe-west1"
  type        = string
}

variable "zone" {
  description = "Zone for the project. Defaults to 'europe-west1-b'"
  default     = "europe-west1-b"
}
variable "billing_account" {
  description = "Billing account for the project."
  default     = ""
  type        = string
}

variable "roles" {
  description = "roles assigned into the project. Due to restrictions in Google API (https://cloud.google.com/resource-manager/reference/rest/v1beta1/projects/setIamPolicy) role/owner must be set through the console and is not allowed int the API"
  default     = {}
  type        = map
}

variable "services" {
  description = "APIs that must be enabled. By default, no API is. Considerer almost compute.googleapis.com and servicenetworking.googleapis.com"
  default = [
    "compute.googleapis.com",
    "sqladmin.googleapis.com",
    "storage-api.googleapis.com",
    "appengine.googleapis.com",
    "appengineflex.googleapis.com",
    "iam.googleapis.com",
    "networkmanagement.googleapis.com",
    "servicenetworking.googleapis.com"
  ]
  type = list(string)
}
