output "billing_account_id" {
	value = data.google_billing_account.account.id
}

output "project_owner" {
	value = google_project_iam_binding.default_roles[*]
}

output "project_id" {
	value = google_project.default.id
}

output "project_number" {
	value = google_project.default.number
}

output "project_name" {
	value = google_project.default.name
}

output "enabled_apis" {
	value = google_project_service.service_apis
}
