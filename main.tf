provider "google" {
  project = var.project_name
  region  = var.region
}

provider "google-beta" {
  project = var.project_name
  region  = var.region
}

data "google_billing_account" "account" {
  billing_account = var.billing_account
}

resource "google_project" "default" {
  name            = var.project_name
  project_id      = var.project_id
  billing_account = data.google_billing_account.account.id
}

resource "google_project_iam_binding" "default_roles" {
	for_each   = var.roles

	project    = google_project.default.project_id
	role       = each.key

	members    = each.value

	depends_on = [google_project.default]
}

resource "google_project_service" "service_apis" {
	for_each = toset(var.services)

	project    = google_project.default.project_id
	service = each.key

	disable_on_destroy = false
}

// data "google_compute_default_service_account" "default" {
// }

// output "default_account" {
//   value = data.google_compute_default_service_account.default.email
// }